import React from 'react';
import { Link } from 'react-router-dom';
import { connectScreenSize } from 'react-screen-size';
import { mapScreenSizeToProps } from '../utils/helpers';

/* About Component */
export default connectScreenSize(mapScreenSizeToProps)(
class About extends React.Component {
	render() {
		const { screen } = this.props;
		return (
			<div className='studyWrapper reviewContainer'>
				<div className='studyContainer'>
					<div className='quizHeader'>
						<div className='quizTitle'>
							<span>About</span>
						</div>
						{!screen.isMobile && <span id="return">
							<Link to='/userSelection'>
								<i className="fa fa-times-circle" aria-hidden="true"></i>
							</Link>
						</span>}
					</div>
					<div className='about'>
						<h1>Flashpoint SightWords</h1>
			            <p>Version 0.1b</p>
			            <p>App created by <b>Team Flashpoint</b> for the Sr. Project at CSUS.</p>
			            <p>It is meant to aid educators in helping their students learn
			            "sight words" - words that a reader should automatically 
			            recognize without picture clues or sounding them out.
			            <br/><br/>
			            Future versions will have integrated audio clips.<br/>		            
			            </p>


			            <Link className='finishBtn' to='/UserSelection'>
							<button>Back</button>
						</Link>
					</div>
				</div>
			</div>
		)
	}
});
